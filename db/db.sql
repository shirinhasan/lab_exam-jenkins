-- movie_id, movie_title, movie_release_date, movie_time, director_name

CREATE TABLE movie (
  movie_id INTEGER PRIMARY KEY auto_increment,
  movie_title VARCHAR(200),
  movie_release_date DATE,
  movie_time TIME,
  director_name VARCHAR(200) 
);
